import math
import copy
from scipy.optimize import curve_fit


def henderson_hasselbalch_for_fitting(ph, hill, pka):
    """Definition of HH equation for fitting"""

    return 1 / (1 + 10**(hill*(pka - ph)))


def compute_pkas(phs, lambda_files):
    """Compute the pkas of all residues in the lambda_files

    This function is a wrapper for curve_fit, to obtain pKa information for
    each residue in an input array of lambda files for a given input array
    of pH values. It returns a list of lists with one entry for each titratable
    residue in the array of lambda files. Each entry contains, in order, the
    Hill coefficient, the error in the Hill coefficient, the pKa,
    and the error in the pKa, with the errors estimated from the fit error.

    A note, as the HH is nonlinear, we have to provide initial guesses for
    the pKa and Hill coefficient. To do this, the algorithm searches for
    the S value closest to 0.5, assuming that the associated pH would be close
    to the pKa. If no S value is within 0.4 of 0.5 or if the fit fails, the method
    returns math.nan for the Hill coefficient, the pKa, and the errors in
    these quantities.

    It does not throw errors for failures to fit, because in practice
    a simulation will have some residues that fail to titrate in the
    pH range but are not interesting in the analysis. These fail to
    fit, but we generally do not care.
    """

    data = []
    for i in range(len(lambda_files[0].s)):
        s_values = [lambda_file.s[i] for lambda_file in lambda_files]
        diffs = [abs(s_value - 0.5) for s_value in s_values]
        min_diff = min(diffs)
        if min_diff < 0.4:
            initial_params = [1, phs[diffs.index(min_diff)]]
            try:
                fit = curve_fit(henderson_hasselbalch_for_fitting, phs,
                                s_values, p0=initial_params)
                data.append([fit[0][0], math.sqrt(fit[1][0][0]), fit[0][1],
                            math.sqrt(fit[1][1][1])])
            except RuntimeError:
                data.append([math.nan, math.nan, math.nan, math.nan])
        else:
            data.append([math.nan, math.nan, math.nan, math.nan])
    return data


class lambda_file:
    """The basic class for holding the information in a lambda file.

    The constructor takes a path to the lambda file. Following is a list of members:

    1. ntitr -- The number of titration variables (1 for each LYS-type residue
    and 2 otherwise for tautomers.)
    2. titrreses -- The residue numbers for each titratable residue.
    Each titratable residue has one entry, regardless of whether it is
    tautomer or not.
    3. titrcols -- Which titration variable corresponds to each titratable
    residue, one entry per residue.
        Eg., lambda_file.titrcols[lambda_file.titrreses.index(resid)] would return
         which entry in a pka data list corresponded to residue resid.
    4. spgrps -- Which residue type is each titratable residue?
    One entry per titratable residue.
    5. steps -- A list that contains how many MD steps correspond to
    each lambda value
    6. lambdavals -- The lambda data, one list for each column in the lambda file.

    By default, the S values and analysis values are not printed. Running compute
    s_values performs the computations.
    compute_s_values takes several optional arguments:
    1. smalllambda -- the cutoff below which a residue is considered protonated.
     Default value = 0.2
    2. biglambda -- The cutoff above which a residue is considered unprotonated.
        Default value = 0.8
    3. minstep -- If set to a positive value, the minimum number of MD steps
    before starting computation of S values.
    4. maxstep -- If set to a positive value, the maximum number of steps for
    computing the S values.
    5. save_running -- Should the running estimates of the S values be computed?
    By default set to false because they take a large amount of memory.

    Running this function creates several new members of the class:
    1. s -- the S values for each titratable residue,
    one for each titratable residue
    2. mixed -- the mixed fraction for each titratable residue,
    one for each titratable residue
    3. micros -- Microscopic pKa's for each titratable residue.
    Each titratable residue gets a list that contains the microscopic pKa's
    if the residue has tautomers.
    4. running_s -- arrays containing the running estimates of S
    for each titratable residue.
    """

    def __init__(self, file_path=''):
        with open(file_path, 'r') as lf:
            line = lf.readline()
            tokens = line.split()
            self.ntitr = int(tokens[-1])
            line = lf.readline()
            tokens = line.split()
            self.titrreses = [int(token) for j, token in enumerate(tokens)
                              if (j > 1 and token != tokens[j - 1])]
            self.titrcols = [j - 1 for j, token in enumerate(tokens)
                             if (j > 1 and token != tokens[j - 1])]
            line = lf.readline()
            tokens = line.split()
            self.spgrps = [int(token) for j, token in enumerate(tokens)
                           if (j > 1 and (int(token) == 0 or int(token) == 1
                                          or int(token) == 3))]
            self.steps = []
            self.ititrs = [titrcol - 1 for titrcol in self.titrcols]
            line = lf.readline()
            self.lambdavals = [[] for j in range(self.ntitr)]
            for line in lf:
                tokens = line.split()
                self.steps.append(int(tokens[0]))
                for j in range(self.ntitr):
                    self.lambdavals[j].append(float(tokens[1 + j]))

    def compute_s_values(self, smalllambda=0.2, biglambda=0.8, minstep=-1,
                         maxstep=-1, save_running=False):
        self.running_s = [[] for titrres in self.titrreses]
        self.s = []
        self.mixed = []
        self.micros = [[] for titrres in self.titrreses]
        total_frames = len(self.lambdavals[0])
        index_min = 0
        index_max = len(self.lambdavals[0])
        if minstep > 0:
            for i, step in enumerate(self.steps):
                if step > minstep:
                    index_min = i
                    break
        if maxstep > 0:
            for i, step in enumerate(self.steps):
                if step > maxstep:
                    index_max = i
                    break
        for i, titrres in enumerate(self.titrreses):
            spgrp = self.spgrps[i]
            titrcol = self.titrcols[i] - 1
            number_protonated = 0
            number_unprotonated = 0
            if spgrp != 0:
                number_protonated_taut1 = 0
                number_unprotonated_taut1 = 0
                number_protonated_taut2 = 0
                number_unprotonated_taut2 = 0
            for j, mylambda in enumerate(self.lambdavals[titrcol]):
                if j >= index_min and j <= index_max:
                    if spgrp == 0:
                        if mylambda < smalllambda:
                            number_protonated += 1
                        elif mylambda > biglambda:
                            number_unprotonated += 1
                    else:
                        myx = self.lambdavals[titrcol + 1][j]
                        if mylambda < smalllambda and myx < smalllambda:
                            number_protonated_taut1 += 1
                            number_protonated += 1
                        elif mylambda < smalllambda and myx > biglambda:
                            number_protonated_taut2 += 1
                            number_protonated += 1
                        elif mylambda > biglambda and myx < smalllambda:
                            number_unprotonated_taut1 += 1
                            number_unprotonated += 1
                        elif mylambda > biglambda and myx > biglambda:
                            number_unprotonated_taut2 += 1
                            number_unprotonated += 1
                    if save_running:
                        if number_protonated + number_unprotonated > 0:
                            self.running_s[i].append(number_unprotonated /
                                                     (number_protonated +
                                                      number_unprotonated))
                        else:
                            self.running_s[i].append(0)
            self.mixed.append((total_frames - number_protonated -
                               number_unprotonated) / total_frames)
            if number_protonated + number_unprotonated > 0:
                self.s.append(number_unprotonated / (number_protonated +
                                                     number_unprotonated))
            else:
                self.s.append(0)
            if spgrp == 1:
                if (number_unprotonated_taut1 + number_protonated) > 0:
                    self.micros[i].append(number_unprotonated_taut1 /
                                          (number_unprotonated_taut1 +
                                           number_protonated))
                else:
                    self.micros[i].append(1.0)
                if number_unprotonated_taut2 + number_protonated > 0:
                    self.micros[i].append(number_unprotonated_taut2 /
                                          (number_unprotonated_taut2 +
                                           number_protonated))
                else:
                    self.micros[i].append(1.0)
            elif spgrp == 3:
                if number_unprotonated + number_protonated_taut1 > 0:
                    self.micros[i].append(number_unprotonated /
                                          (number_unprotonated +
                                           number_protonated_taut1))
                else:
                    self.micros[i].append(1.0)
                if number_unprotonated + number_protonated_taut2 > 0:
                    self.micros[i].append(number_unprotonated /
                                          (number_unprotonated +
                                           number_protonated_taut2))
                else:
                    self.micros[i].append(1.0)


class rem_file:
    """An object to hold the replica-exchange log file information.

    It takes two arguments, the path to the log file and the swap frequency
    in the simulation. It creates an internal member, phs, that contains
    the phs of each replica after each swap attempt. Really, users should
    not normally need the internals of this object, just using it for a wrap
    lambdas call.
    """

    def __init__(self, file_path, swap_freq):
        self.swap_freq = swap_freq
        self.phs = []
        with open(file_path, 'r') as rem:
            while True:
                line = rem.readline()
                if 'exchange' in line:
                    break
            temp_orig_phs = []
            temp_phs = []
            while True:
                line = rem.readline()
                if 'exchange' in line:
                    break
                temp_orig_phs.append(float(line.split()[2]))
                temp_phs.append(float(line.split()[3]))
            self.phs.append(temp_phs)
            while True:
                temp_phs = []
                for i in range(len(self.phs[0])):
                    line = rem.readline()
                    if line == '':
                        break
                    temp_phs.append(float(line.split()[3]))
                line = rem.readline()
                if line == '' and len(temp_phs) != len(self.phs[-1]):
                    break
                self.phs.append(temp_phs)


def wrap_lambda_files(remfile, lambda_file_array, ph_array, smalllambda=0.2,
                      biglambda=0.8, minstep=-1, maxstep=-1):
    """Wrap the lambda files

    This function takes a remfile object, containing the replica-exchange
    information, a list of lambda_file objects from the replica-exchange
    simulation, an array of pH values at which the simulation was run,
    and optional arguments smalllambda, biglambda, minstep, and maxstep,
    which are as defined above. This function returns a new list of lambda
    files, with each file corresponding to one of the pH values.
    """

    wrapped_lambda_files = copy.deepcopy(lambda_file_array)
    for file in wrapped_lambda_files:
        file.lambdavals = [[] for i in range(len(lambda_file_array[0].lambdavals))]
        file.steps = []
    for i, step in enumerate(lambda_file_array[0].steps):
        if step <= len(remfile.phs) * remfile.swap_freq:
            rem_index = math.floor(step / remfile.swap_freq)
            if rem_index == step / remfile.swap_freq:
                rem_index = rem_index - 1
            for j, file in enumerate(wrapped_lambda_files):
                file.steps.append(step)
                rep_index = remfile.phs[rem_index].index(ph_array[j])
                for k in range(len(file.lambdavals)):
                    file.lambdavals[k].append(lambda_file_array[rep_index].lambdavals[k][i])
    for file in wrapped_lambda_files:
        file.compute_s_values(smalllambda, biglambda, minstep, maxstep)
    return wrapped_lambda_files
